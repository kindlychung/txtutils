#pragma once

#include <stdio.h>
#include <sys/stat.h>
#include <string>
off_t fileSize(std::string fn);
