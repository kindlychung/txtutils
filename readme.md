# Requirements

* git
* R packages:
    * Rcpp
    * devtools

# Installation

    require(devtools)
    Sys.setenv("PKG_CXXFLAGS"="-std=c++11")
    install_bitbucket("kindlychung/txtutils")

# Changes

* some printing functions for debugging

# To do
